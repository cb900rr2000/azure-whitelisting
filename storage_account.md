# Microsoft.Storage/StorageAccounts - Non Data Lake Features

**Definition Version:** v.0.0.1

**Release Date:** 2023-05-16T14:54:53Z

**Data At Rest Classification:** Highly Restricted

**Data At Rest Rationale:** Support for Customer Managed Key (CMK), Infrastructure at rest encryption, Blob Client-Side Encryption and Disable Public Access

**SLA:** 99.9%

**Data Redundancy:** Geo-Zone-Redundant-Storage

#### Storage Account Usecase 1
Client-Side Encryption for Azure Storage Account Blobs and Azure Key Vault for Microsoft Azure Storage (.NET)

The Azure Storage Client Library for .NET supports encrypting data within client applications before uploading to Azure Storage, and decrypting data while downloading to the client. The library also supports integration with Azure Key Vault

This use case follows the following sequence;
Encryption

- The  .NET storage library generates a Content Encryption Key (CEK) which is a one-time-use symmetric key
- The Blob data is encrypted with this CEK
- The Storage .NET library interacts with Key Vault with the wrap and unwrap operations. The application using the library uses its host compute services managed identity to authenticate with the Key Vault
- The CEK is then encrypted (wrapped) with a Key Encryption Key (KEK) asymmetric HSM backed Key that is created and managed by Azure Key Vault.
- The encrypted data is then uploaded to the Azure Storage Account. The wrapped key along with some additional encryption metadata is stored as a blob.

Decryption

- The .Net storage library downloads the encrypted data along with the encryption material from the Storage Account
- The CEK is the unwrapped (decrypted) using the KEK by invoking the Key Vault unwrap operation
- The CEK is then used to decrypt the encrypted data



|**Data Classification**|**Key Vault Key Type**|
|--|--|
|Highly Restricted|RSA-HSM backed KEK|
|Internal|No encryption required|
|Public|No encryption required|


![Picture1](uploads/d2c238a3c3bb62e211039002fc72ec3d/Picture1.png)



---

### Configuration Management


##### Tag Storage Accounts in accordance with XXXX policy

**AM-6_SA_0015**

**Control Discussion:** To provide Security Operations the ability in the case of an security incident to understand the classification of the data that maybe impacted

**Control Owner:** SIA



**SCF Sub-Objective** 000-000-00-00


**Resource Technical Control Configuration** True

**Azure Policy**

**Policy Name:** N/A

**Policy ID:** N/A

**Effect:** N/A

**Required Effect:** Audit

**Policy Possible:** True


---

##### Enable Soft Delete for Blob

**BR_2_SA_0012**

**Control Discussion:** To enable data recovery in the event of a malicious or accidental deletion

**Control Owner:** SIA



**SCF Sub-Objective** 000-000-00-00


**Resource Technical Control Configuration** True

**Azure Policy**

**Policy Name:** N/A

**Policy ID:** N/A

**Effect:** N/A

**Required Effect:** N/A

**Policy Possible:** True


---

##### Enable Soft Delete for Files

**BR_2_SA_0013**

**Control Discussion:** To enable data recovery in the event of a malicious or accidental deletion

**Control Owner:** SIA



**SCF Sub-Objective** 000-000-00-00


**Resource Technical Control Configuration** True

**Azure Policy**

**Policy Name:** N/A

**Policy ID:** N/A

**Effect:** N/A

**Required Effect:** N/A

**Policy Possible:** True


---
### Access Control


##### Disable Storage Account cross tenant replication

**DP_2_SA_0009**

**Control Discussion:** Prevent the replication of Storage Account objects to a non XXXX Azure tenant

**Control Owner:** SIA



**SCF Sub-Objective** 000-000-00-00


**Resource Technical Control Configuration** True

**Azure Policy**

**Policy Name:** Storage accounts should prevent cross tenant object replication

**Policy ID:** /providers/Microsoft.Authorization/policyDefinitions/92a89a79-6c52-4a7e-a03f-61306fc49312

**Effect:** Audit

**Required Effect:** Deny

**Policy Possible:** True


---

##### Use System Assigned Managed Identity for CMK Key Vault access

**DP_8_SA_0016**

**Control Discussion:** A user assigned identity provides the ability for an identity to have access to multiple Storage Accounts CMK’s increasing the blast radius in the case of compromise

**Control Owner:** SIA



**SCF Sub-Objective** 000-000-00-00


**Resource Technical Control Configuration** True

**Azure Policy**

**Policy Name:** N/A

**Policy ID:** N/A

**Effect:** N/A

**Required Effect:** Audit

**Policy Possible:** False


---

##### Disable storage account keys

**IM_1_SA_0005**

**Control Discussion:** AAD authentication with RBAC and conditional access polices provides superior security

**Control Owner:** SIA



**SCF Sub-Objective** 000-000-00-00

**Defined In MCSB** True

**Resource Technical Control Configuration** True

**Azure Policy**

**Policy Name:** Storage accounts should prevent shared key access

**Policy ID:** /providers/Microsoft.Authorization/policyDefinitions/8c6a50c6-9ffd-4ae7-986f-5fa6111f9a54

**Effect:** Audit

**Required Effect:** Deny

**Policy Possible:** True


---

##### Use Managed Identities where possible for services or applications authenticating to a Storage Account

**IM_3_SA_0006**

**Control Discussion:** Managed Identities do not require the management of secrets or certificates, providing superior security

**Control Owner:** SIA



**SCF Sub-Objective** 000-000-00-00

**Defined In MCSB** True


**Azure Policy**

**Policy Name:** N/A

**Policy ID:** N/A

**Effect:** N/A

**Required Effect:** N/A

**Policy Possible:** False


---

##### Disable Public Network Access

**NS_2_SA_0004**

**Control Discussion:** Removes the possibility of accidental or intended public exposure of data

**Control Owner:** SIA



**SCF Sub-Objective** 000-000-00-00

**Defined In MCSB** True

**Resource Technical Control Configuration** True

**Azure Policy**

**Policy Name:** Storage accounts should disable public network access

**Policy ID:** /providers/Microsoft.Authorization/policyDefinitions/b2982f36-99f2-4db5-8eff-283140c09693

**Effect:** Audit

**Required Effect:** Deny

**Policy Possible:** True


---

##### Use least privilege RBAC roles for Storage Account authorisation

**PA_7_SA_0014**

**Control Discussion:** Using roles like Storage Blob Data Reader reduces the blast radius should an identity become compromised

**Control Owner:** SIA



**SCF Sub-Objective** 000-000-00-00

**Defined In MCSB** True


**Azure Policy**

**Policy Name:** N/A

**Policy ID:** N/A

**Effect:** N/A

**Required Effect:** N/A

**Policy Possible:** False


---
### System and Communication Protection


##### Require control plane secure transfer for REST API operations

**DP_3_SA_0001**

**Control Discussion:** Using non encrypted http API calls could allow malicious capture of XXXX sensitive information

**Control Owner:** SIA



**SCF Sub-Objective** 000-000-00-00


**Resource Technical Control Configuration** True

**Azure Policy**

**Policy Name:** N/A

**Policy ID:** N/A

**Effect:** N/A

**Required Effect:** Deny

**Policy Possible:** True


---

##### Require data plane secure data transfer

**DP_3_SA_0002**

**Control Discussion:** HTTPS ensures authentication between the server and the service and protects data in transit from network layer attack

**Control Owner:** SIA



**SCF Sub-Objective** 000-000-00-00


**Resource Technical Control Configuration** True

**Azure Policy**

**Policy Name:** Secure transfer to storage accounts should be enabled

**Policy ID:** /providers/Microsoft.Authorization/policyDefinitions/404c3081-a854-4457-ae30-26a93ef643f9

**Effect:** Audit

**Required Effect:** Deny

**Policy Possible:** True


---

##### Use a minimum of TLS version 1.2 with HTTPS

**DP_3_SA_0003**

**Control Discussion:** Previous versions of TLS are vulnerable to being exploited

**Control Owner:** SIA



**SCF Sub-Objective** 000-000-00-00

**Defined In MCSB** True

**Resource Technical Control Configuration** True

**Azure Policy**

**Policy Name:** Storage accounts should have the specified minimum TLS version

**Policy ID:** /providers/Microsoft.Authorization/policyDefinitions/fe83a0eb-a853-422d-aac2-1bffd182c5d0

**Effect:** Audit

**Required Effect:** Deny

**Policy Possible:** True


---

##### Use a CMK for encryption persisted in an HSM backed Key Vault for all Storage Account scopes

**DP_3_SA_0007**

**Control Discussion:** If the Microsoft managed service level key is compromised XXXX can revoke access to data

**Control Owner:** SIA



**SCF Sub-Objective** 000-000-00-00

**Defined In MCSB** True

**Resource Technical Control Configuration** True

**Azure Policy**

**Policy Name:** Storage accounts should use customer-managed key for encryption

**Policy ID:** /providers/Microsoft.Authorization/policyDefinitions/6fac406b-40ca-413b-bf8e-0bf964659c25

**Effect:** Audit

**Required Effect:** Deny

**Policy Possible:** True


---

##### Enable infrastructure encryption

**DP_3_SA_0008**

**Control Discussion:** A second Microsoft managed key encryption at rest layer, reduces the likelihood of Microsoft’s encryption being compromised

**Control Owner:** SIA



**SCF Sub-Objective** 000-000-00-00


**Resource Technical Control Configuration** True

**Azure Policy**

**Policy Name:** N/A

**Policy ID:** N/A

**Effect:** N/A

**Required Effect:** Deny

**Policy Possible:** True


---
### Assessment Authorisation and Monitoring


##### Enable Defender for Cloud at a subscription level

**LT_4_SA_0010**

**Control Discussion:** Defender for Cloud offers advance threat detection capabilities to be used by the Security Operations Centre

**Control Owner:** SIA



**SCF Sub-Objective** 000-000-00-00

**Defined In MCSB** True

**Resource Technical Control Configuration** True

**Azure Policy**

**Policy Name:** N/A

**Policy ID:** N/A

**Effect:** N/A

**Required Effect:** Audit

**Policy Possible:** True


---
### Audit and Accountability


##### Send all diagnostic logs to a central Log Analytics workspace

**LT_4_SA_0011**

**Control Discussion:** To provide logs for possible security investigation and to provide the ability if required to have a custom security alert

**Control Owner:** SIA



**SCF Sub-Objective** 000-000-00-00

**Defined In MCSB** True

**Resource Technical Control Configuration** True

**Azure Policy**

**Policy Name:** N/A

**Policy ID:** N/A

**Effect:** N/A

**Required Effect:** N/A